#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/default.mk
ifneq (,$(filter mipsel ,$(DEB_BUILD_ARCH)))
export CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS | sed 's/-O[1-9]//') -O0
export CFLAGS:=$(shell dpkg-buildflags --get CFLAGS | sed 's/-O[1-9]//') -O0
endif

%:
	dh $@

ifneq (,$(filter mipsel ,$(DEB_BUILD_ARCH)))
override_dh_auto_configure:
	dh_auto_configure -- --enable-debug --without-openmp
endif

override_dh_auto_build-indep:
	# nothing to do here

override_dh_auto_test-indep:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	echo "Do not run build time test"
endif

override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_auto_test -a
	# Run additional test from python-biopython package to verify that
	# this will work as well
	src/clustalo -i debian/tests/biopython_testdata/f002 --guidetree-out temp_test.dnd -o temp_test.aln --outfmt clustal --force
	rm temp_test.aln  temp_test.dnd
endif

override_dh_auto_install-arch:
	dh_auto_install -a
	rm -rf debian/tmp/usr/lib/pkgconfig
	rm -f debian/tmp/usr/lib/*/libclustalo.la
	mv debian/tmp/usr/include debian/libclustalo-dev/usr
	mv debian/tmp/usr/lib debian/libclustalo-dev/usr
	mv debian/tmp/usr/bin debian/clustalo/usr

override_dh_auto_install-indep:
	# Generate doxygen doc
	mkdir -p doc
	doxygen Doxyfile
	find doc -name "*.md5" -delete

override_dh_clean:
	dh_clean
	rm -f  _configs.sed
	rm -rf doc
